//
//  ViewController.m
//  PickerViewSlot
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/30.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /*
     三個陣列對應儲存3個component
     */
    dataAry1=[NSMutableArray new];

    dataAry2=[NSMutableArray new];

    dataAry3=[NSMutableArray new];
    
    //使delegate與ViewController發生關聯，同MainStoryboard拖拉方式
    _pickerView.delegate = self;
  
  //建議這麼做！因為直接存取@property很容易發生兩thread同時存取同個@property而導致同個變數下發生同期同分的問題。
  self.pickerView.delegate = self;
  
    //使dataSource與ViewController發生關聯，同MainStoryboard拖拉方式
    _pickerView.dataSource = self;
    
    /*
     用for回權把資料灌進去，資料是產出的亂數0~9並塞進陣列裏，藉由數字對應圖片
     */
    for (NSInteger i = 0; i<100; i++) {
        [dataAry1 setObject:[NSNumber numberWithInt:arc4random()%10] atIndexedSubscript:i];
        [dataAry2 setObject:[NSNumber numberWithInt:arc4random()%10] atIndexedSubscript:i];
        [dataAry3 setObject:[NSNumber numberWithInt:arc4random()%10] atIndexedSubscript:i];
    }
    //在程式載入階段執行拉霸的動作
    [self action];
}

/*
 
 */
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    //先建立一個UIImageView
    UIImageView *slotImage=[[UIImageView alloc]init];
    /*
     接著在每個component對應設定不同圖片，圖片流水號0~9，由該component對應的陣列對應的列號所決定。並透過integerValue強制轉型態。
     */
    if (component==0) {
        [slotImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"slot%ld.png", (long)[[dataAry1 objectAtIndex:row] integerValue]]]];
    }else if (component==1){
        [slotImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"slot%ld.png", (long)[[dataAry2 objectAtIndex:row] integerValue]]]];
    }else if(component==2){
        [slotImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"slot%ld.png", (long)[[dataAry3 objectAtIndex:row] integerValue]]]];
    }
    
    return slotImage;
}


/*
 
 */
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (component == 0) {
        return [dataAry1 objectAtIndex:row];
    } else if(component == 1){
        return [dataAry2 objectAtIndex:row];
    } else{
        return [dataAry3 objectAtIndex:row];
    }
    
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

/*
 每個滾軸做100個row，這樣才會出現旋轉的效果
 */
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    return 100;
    
    /*
    if (component==0) {
        return [dataAry1 count];
    } else if(component==1){
        return [dataAry2 count];
    }else{
        return [dataAry3 count];
    }
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)action{
    /*
     我們希望按下按鈕之後做旋轉的動作，selectedRow  inComponent  animated 透過這個來決定要轉去哪，是哪個component要轉，是否有動畫效果
     94+3的用意是...mod 94 得到0~93，+3會得到3~96。目的是不要讓他選到太邊邊的元素避免有空白處出現。
     */
    [_pickerView selectRow:arc4random()%94+3 inComponent:0 animated:YES];
    [_pickerView selectRow:arc4random()%94+3 inComponent:1 animated:YES];
    [_pickerView selectRow:arc4random()%94+3 inComponent:2 animated:YES];
    
    /*
     判斷如果第一個陣列所選的項目是否與第二個相同，第二個陣列所選的陣列元素是否跟第三個相同。如果通通成立，則顯示Bingo。
     */
    if ([dataAry1 objectAtIndex:[_pickerView selectedRowInComponent:0]]==[dataAry2 objectAtIndex:[_pickerView selectedRowInComponent:1]] &&
        [dataAry2 objectAtIndex:[_pickerView selectedRowInComponent:1]]==[dataAry3 objectAtIndex:[_pickerView selectedRowInComponent:2]]) {
        _labelShow.text = @"Bingo!";
    }else{
        _labelShow.text = @"繼續努力";
    }
}

// returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return 100.0f;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 100.0f;
}

- (IBAction)toPicker:(id)sender {
    [self action];
}
@end
